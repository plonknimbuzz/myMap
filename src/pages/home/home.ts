import { Component } from '@angular/core';
import { NavController } from 'ionic-angular';
import { HttpClient } from '@angular/common/http';

@Component({
	selector: 'page-home',
	templateUrl: 'home.html'
})
export class HomePage {
	map : any;
	constructor(
		public navCtrl: NavController, 
  		private http: HttpClient
  	) {

	}

  	ionViewDidEnter(){
		let latLng = new google.maps.LatLng(-6.952205,107.631984);
	    let mapOptions = {
			center: latLng,
			zoom: 10,
			mapTypeId: google.maps.MapTypeId.ROADMAP,
			streetViewControl: false,
			disableDefaultUI: true
	    }
	    this.map = new google.maps.Map(document.getElementById('map'), mapOptions);
	    this.getMarkers(this.map);
	}

	getMarkers(map) {
		var markers = [];
		var infowindow = new google.maps.InfoWindow();
		this.http.get('http://testing.indomobilebusiness.com/api/teslatlong.php').subscribe(function(res){
			console.log(res);
			for (let i = 0; i < res.length; i++) {  
				let loc = res[i].latlong.split(',');  
				markers[i]= new google.maps.Marker({
					// position: new google.maps.LatLng(locations[i][1], locations[i][2]),
					position: new google.maps.LatLng(loc[0],loc[1]),
					map: map,
					label: res[i].id
				});
				google.maps.event.addListener(markers[i], 'click', (function (marker, nama) {
					return function () {
						infowindow.setContent(nama);
						infowindow.open(this.map, marker);
					}
				})(markers[i], res[i].nama));
		    }
		});
	
	}
}
